#!/usr/bin/python
import os
import importlib
import smtplib
import sys
import json, requests
import time  # for sleep
import logging  # for logs
import logging.handlers  # for logs
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

import fileinput




#logging and config
sys.path.insert(0, './workers')
LEVEL = logging.DEBUG  # Pick minimum level of reporting logging - debug OR info
# Format to include when, who, where, what
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# Name is module name
logger = logging.getLogger(__name__)
logger.setLevel(LEVEL)
# Create file size limit and file name
handler = logging.handlers.RotatingFileHandler('debug.log', maxBytes=2000000, backupCount=10)
handler.setLevel(LEVEL)
handler.setFormatter(formatter)
# Add handler to logger
logger.addHandler(handler)
################################################

#sendAlert function
def sendAlert(worker):

    msg = MIMEMultipart()
    msg['From'] = 'coffeeminerboy@gmail.com'
    msg['To'] = 'coffeeminerboy@gmail.com'
    msg['Subject'] = 'Worker' + str(worker) + ' reset'
    message = 'here is the email'
    msg.attach(MIMEText(message))

    s = smtplib.SMTP('smtp.gmail.com',587)
    s.ehlo()
    # secure our email with tls encryption
    s.starttls()
    # re-identify ourselves as an encrypted connection
    s.ehlo()
    s.login('coffeeminerboy@gmail.com', 'nfrihvzezqixppah')
    s.sendmail('coffeeminerboy@gmail.com', 'coffeeminerboy@gmail.com', msg.as_string())
    s.quit()
    logger.debug("Sent email alert")

def getNiceHashReport(btcAddress):
    url = 'https://api.nicehash.com/api'
    #workers endpoint
    params = dict(
        method='stats.provider.workers',
        addr=btcAddress)
    try:
        logger.debug(
            "Getting worker data from https://api.nicehash.com/api?method=stats.provider.workers&addr=" + btcAddress)
        resp = requests.get(url=url, params=params)
        stats = json.loads(resp.text)
        upworkers = []
        for i in stats["result"]["workers"]:
            upworkers.append(i[0])
        return upworkers
    except:
        logging.error(e)

def resetWorker(config, downworkers, sleep):
       # sendAlert(worker + " seems down! Going to attempt to fix it!",
        #           worker + " seems down! Going to attempt to fix it!",
        #           config.btcAddress, config.walletName, config.email, worker)
    downworkers = downworkers
    logger.info("Powering off " + downworkers)
    wemodownString = "wemo switch " + downworkers + " off"
    logger.info("WEMO DOWN STRING: " + str(wemodownString))
    os.system(wemodownString)
    logger.info("Sleeping "+str(sleep))
    # power wemo up
    time.sleep(sleep)
    logger.info("Powering on " + downworkers)
    wemoupString = "wemo switch " + downworkers + " on"
    logger.info("WEMO UP STRING: " + str(wemoupString))
    os.system(wemoupString)
    logger.info("Sending email")
    sendAlert(downworkers)

def checkWorkers():
    # import config
    config = importlib.import_module("config1")
    # get upworkers
    nicehashreport = getNiceHashReport(config.btcAddress)
    if len(nicehashreport) == 2:
        logger.info("Everyone is up!")
    elif len(nicehashreport) == 1:
        for line in fileinput.input('deadcount', inplace=1):
            if int(line) < 2:
                logger.info("Worker has been down the last "+str(line) + "checks. Sleeping and checking again in 10")
                print(line.replace(line, str(int(line)+1))),
                line = line.replace("\n", "")
            else:
                upworkers = nicehashreport
                downworkerset = set(upworkers) ^ set(config.workers)
                downworker = downworkerset.pop()
                logger.debug("Down worker: " + downworker + " has been down for 30 minutes. Resetting")
                resetWorker(config, downworker, 20)
                print(line.replace(line, str(0))),
                line = line.replace("\n", "")
    elif len(nicehashreport) > 2:
        logging.info("Weird condition")
        if "BigNode1" in nicehashreport and "BigNode2" in nicehashreport:
            logger.info("Everyone is up!")
            for line in fileinput.input('deadcount', inplace=1):
                print(line.replace(line, str(0))),
                line = line.replace("\n", "")
        else:
            logger.info("Check miners!")
    elif len(nicehashreport) < 1:
        resetWorker(config, 'BigNode1', 20)
        time.sleep(60)
        resetWorker(config, 'BigNode2', 20)
        for line in fileinput.input('deadcount', inplace=1):
            print(line.replace(line, str(0))),
            line = line.replace("\n", "")
        sendAlert("BigNode1 and BigNode2")
try:
   checkWorkers()

except Exception as e:
    logger.exception("Something bad happened")
